const puppeteer = require('puppeteer');
const $ = require('cheerio');
const leanderURL = "https://www.applitrack.com/leander/onlineapp/jobpostings/view.asp?internaltransferform.Url&category=&all=1";
const austinURL = "https://www.applitrack.com/austinisd/onlineapp/default.aspx?all=1";
const roundrockURL = "https://ess.roundrockisd.org/ESS/employmentopportunities/default.aspx";
const laketravisURL = "https://www.applitrack.com/laketravis/onlineapp/jobpostings/view.asp?internaltransferform.Url=&all=1";

const SchoolDistrictURL = {
    AUSTIN : austinURL,
    LEANDER : leanderURL,
    ROUNDROCK : roundrockURL,
    LAKETRAVIS : laketravisURL
};

// this seems ineffecient to define these all by hand
var AustinISD = {
    name : "Austin",
    url : SchoolDistrictURL.AUSTIN,
    appliTrack : true
};
var LeanderISD = {
    name : "Leander",
    url : SchoolDistrictURL.LEANDER,
    appliTrack : true
};
var RoundRockISD = {
    name : "Round Rock",
    url : SchoolDistrictURL.ROUNDROCK,
    appliTrack : false
};
var LakeTravisISD = {
    name : "Lake Travis",
    url : SchoolDistrictURL.LAKETRAVIS,
    appliTrack : true
};


var AllSchools = [AustinISD, LeanderISD, RoundRockISD, LakeTravisISD];

const ARTREGEX = /\bart\b/gi;

var JOBS = [];

async function Scrape(currentSchool) {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto(currentSchool.url);
    const content = await page.content();
    var results = ScrapeHTML(content, currentSchool);
    await browser.close();
    return results;
}

function ScrapeHTML(html, currentSchool)
{
    var foundResults = [];
    if (currentSchool.appliTrack)
    {
        $('.postingsList',html).each(function()
        {
            var result = {};
            var title = $('.title',this).text();
            var jobID = $('.title .title2', this).text();
            var idRegex = /\d+/gi;
            var containsArt = ARTREGEX.test(title);
            if (containsArt)
            {
                result.district = currentSchool.name;
                result.postTitle = (title.substring(0,title.toLowerCase().indexOf("jobid")).trim());
                var cleanID = idRegex.exec(jobID.trim())[0];
                result.jobID = (cleanID);
                jobInfo = ParseAppliTrackInfo(this);
                result.jobLocation = jobInfo.location;
                result.datePosted = jobInfo.datePosted;
                result.applicationUrl = ("https://www.applitrack.com/austinisd/onlineapp/_application.aspx?posJobCodes=" + cleanID);
                foundResults.push(result);
            }
        });
    }
    else
    {
        $('.employmentopportunity tbody tr',html).each(function()
        {
            var result = {};
            var entries = $('td p',this).toArray();
            var title = entries[0];
            var code = entries[1];
            var location = entries[2];
            var datePosted = entries[4];
            var idRegex = /(\d+.\d+)/gi;
            var containsArt = ARTREGEX.test($(title).text());
            if (containsArt)
            {
                result.district = currentSchool.name;
                result.postTitle = ($(title).text());
                var cleanID = idRegex.exec($(code).text())[0]
                result.jobID = cleanID;
                result.jobLocation = ($(location).text());
                result.jobLocation = result.jobLocation.substring(result.jobLocation.indexOf(":")+1).trim();
                result.datePosted = ($(datePosted).text());
                result.datePosted = result.datePosted.substring(result.datePosted.indexOf(":")+1).trim();
                result.applicationUrl = ("https://ess.roundrockisd.org/ESS/ApplicantLogin.aspx?returnurl=~/EmploymentOpportunities/ApplicationEntry.aspx&form=CERT&req=" + cleanID)
                foundResults.push(result);
            }
        });
    }
    foundResults = [...new Set(foundResults)];
    return foundResults;
}

function ParseAppliTrackInfo(html)
{
    var result = {};
    $('div li', html).each(function()
    {
        var spans = $('span',this);
        // this is a bit hack since I am doing a direct string compare
        var infoTitle = $(spans[0]).text().trim();
        if (infoTitle.toLowerCase().includes("date posted"))
        {
            result.datePosted = $(spans[1]).text().trim();
        }
        if (infoTitle.toLowerCase().includes("location"))
        {
            result.location = $(spans[1]).text().trim();
        }
    })
    return result;
}

async function ScrapAll()
{
    for(school in AllSchools)
    {
        var currentSchool = AllSchools[school];
        console.log("Checking %s School District", currentSchool.name);
        var results = await Scrape(currentSchool);
        console.log("Found %i jobs", results.length);
        console.log(results);
        // JOBS.push(results);
    }
}
ScrapAll().then(res => console.log("Done"));
