const puppeteer = require('puppeteer');
const url = 'https://www.reddit.com';
const $ = require('cheerio');
const leanderURL = "https://www.applitrack.com/leander/onlineapp/jobpostings/view.asp?internaltransferform.Url&category=&all=1";
const austinURL = "https://www.applitrack.com/austinisd/onlineapp/default.aspx?all=1";
const roundrockURL = "https://ess.roundrockisd.org/ESS/employmentopportunities/default.aspx";

const schoolDistrict = {
    AUSTIN : austinURL,
    LEANDER : leanderURL,
    ROUNDROCK : roundrockURL
}

// LEANDER
puppeteer
  .launch()
  .then(function(browser) {
    return browser.newPage();
  })
  .then(function(page) {
    return page.goto(leanderURL).then(function() {
      return page.content();
    });
  })
  .then(function(html) {
    $('.postingsList',html).each(function()
    {
      var title = $('.title',this).text();
      var jobID = $('.title .title2', this).text();
      var regex = /\barts\b|\bart\b/gi;
      var containsArt = regex.test(title);
      if (containsArt)
      {
        console.log("Leander");
        console.log(title);
        console.log(jobID);
      }
    });
  })
  .catch(function(err) {
    //handle error
  });

// AUSTIN
puppeteer
  .launch()
  .then(function(browser) {
    return browser.newPage();
  })
  .then(function(page) {
    return page.goto(austinURL).then(function() {
      return page.content();
    });
  })
  .then(function(html) {
    $('.postingsList',html).each(function()
    {
      var title = $('.title',this).text();
      var jobID = $('.title .title2', this).text();
      var regex = /\barts\b|\bart\b/gi;
      var containsArt = regex.test(title);
      if (containsArt)
      {
        console.log("Austin");
        console.log(title);
        console.log(jobID);
      }
    });
  })
  .catch(function(err) {
    //handle error
  });

// ROUND ROCK
puppeteer
 .launch()
 .then(function(browser) {
   return browser.newPage();
 })
 .then(function(page) {
   return page.goto(roundrockURL).then(function() {
     return page.content();
   });
 })
 .then(function(html) {
   $('.employmentopportunity tbody tr',html).each(function()
   {
      var entries = $('td p',this).toArray();
      var title = entries[0];
      var code = entries[1];
      var location = entries[2];
      var regex = /\barts\b|\bart\b/gi;
      var containsArt = regex.test($(title).text());
      if (containsArt)
      {
        console.log("Round Rock");
        console.log($(title).text());
        console.log($(code).text());
        console.log($(location).text());
      }
    });
 })
 .catch(function(err) {
   //handle error
 });
